﻿using System.Web.Http.Results;
using Moq;
using NUnit.Framework;

using Iasset.TechnicalTest.WebAPI.Controllers;
using Iasset.TechnicalTest.Business.Interfaces;
using Iasset.TechnicalTest.Objects;

namespace Iasset.TechnicalTest.WebAPI.Tests.Controllers
{
    [TestFixture]
    public class CountryControllerTests
    {
        private CountryController countryController;
        private Mock<ICountryService> _countryService;

        private CitiesResult _citiesResult;

        [SetUp]
        public void SetUp()
        {
            _citiesResult = new CitiesResult { Cities = new City[] { new City { Name = "Cairns Airport" } } };

            _countryService = new Mock<ICountryService>();

            _countryService.Setup(service => service.GetCities("Australia")).Returns(_citiesResult);

            countryController = new CountryController(_countryService.Object);
        }

        [Test]
        public void GetCities_WhenCalled_ReturnTypeOkResultWithCitiesResult()
        {
            // Act
            var result = countryController.GetCities("Australia");

            // Assert
            Assert.That(result, Is.Not.Null);

            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<CitiesResult>>());            
        }

        [Test]
        public void GetCities_WhenCalled_CountryService_GetCitiesCalled()
        {
            // Act
            countryController.GetCities("Australia");
            // Assert
            _countryService.Verify(s => s.GetCities("Australia"));
        }

        [Test]
        public void GetCities_WhenCalledByPassingCountryAustralia_ReturnOkResultwithMockData()
        {
            // Act
            var result = countryController.GetCities("Australia") as OkNegotiatedContentResult<CitiesResult>;

            // Assert
            Assert.That(result.Content, Is.EqualTo(_citiesResult));
        }
    }
}
