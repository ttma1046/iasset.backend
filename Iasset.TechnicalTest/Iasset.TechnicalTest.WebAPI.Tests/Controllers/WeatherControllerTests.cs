﻿using System.Web.Http.Results;
using Moq;
using NUnit.Framework;

using Iasset.TechnicalTest.WebAPI.Controllers;
using Iasset.TechnicalTest.Business.Interfaces;
using Iasset.TechnicalTest.Objects;

namespace Iasset.TechnicalTest.WebAPI.Tests.Controllers
{
    [TestFixture]
    public class WeatherControllerTests
    {
        private WeatherController weatherController;
        private Mock<IWeatherService> _weatherService;

        private Weather _weatherResult;

        [SetUp]
        public void SetUp()
        {
            _weatherResult = new Iasset.TechnicalTest.Objects.Weather
            {
                Location = "Sydney"
            };

            _weatherService = new Mock<IWeatherService>();

            _weatherService.Setup(service => service.GetWeatherByCity("Sydney")).Returns(_weatherResult);

            weatherController = new WeatherController(_weatherService.Object);
        }

        [Test]
        public void GetWeatherByCity_WhenCalled_ReturnTypeOkResultWithWeather()
        {               
            // Act
            var result = weatherController.GetWeatherByCity("Sydney");

            // Assert
            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<Weather>>());
        }

        [Test]
        public void GetWeatherByCity_WhenCalled_WeatherService_GetWeatherByCityCalled()
        {
            // Act
            weatherController.GetWeatherByCity("Sydney");
            // Assert
            _weatherService.Verify(s => s.GetWeatherByCity("Sydney"));
        }

        [Test]
        public void GetWeatherByCity_WhenCalledByPassingCitySydney_ReturnOkResultwithMockData()
        {
            // Act
            var result = weatherController.GetWeatherByCity("Sydney") as OkNegotiatedContentResult<Weather>;

            // Assert
            Assert.That(result.Content, Is.EqualTo(_weatherResult));
        }
    }
}
