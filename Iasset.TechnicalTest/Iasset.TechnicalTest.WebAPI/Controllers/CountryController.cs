﻿namespace Iasset.TechnicalTest.WebAPI.Controllers
{
    using System.Web.Http;

    using Iasset.TechnicalTest.Business.Interfaces;

    [RoutePrefix("api/country")]
    public class CountryController : ApiController
    {
        private ICountryService CountryService { get; set; }

        public CountryController(ICountryService countryService)
        {
            this.CountryService = countryService;
        }

        [HttpGet, Route("{countryName}/cities")]
        public IHttpActionResult GetCities(string countryName)
        {
            var result = this.CountryService.GetCities(countryName);

            return Ok(result);
        }
    }
}
