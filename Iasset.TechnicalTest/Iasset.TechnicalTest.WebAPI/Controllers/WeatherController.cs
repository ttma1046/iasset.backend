﻿namespace Iasset.TechnicalTest.WebAPI.Controllers
{
    using System.Web.Http;

    using Iasset.TechnicalTest.Business.Interfaces;

    [RoutePrefix("api/weather")]
    public class WeatherController : ApiController
    {
        private IWeatherService WeatherService { get; set; }

        public WeatherController(IWeatherService weatherService)
        {
            this.WeatherService = weatherService;
        }

        [HttpGet, Route("{cityName}")]
        public IHttpActionResult GetWeatherByCity(string cityName)
        {
            var result = this.WeatherService.GetWeatherByCity(cityName);

            return Ok(result);
        }
    }
}
