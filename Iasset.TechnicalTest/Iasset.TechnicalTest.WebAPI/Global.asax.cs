﻿namespace Iasset.TechnicalTest.WebAPI
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Bootstrap.AutoMapper;
    using Bootstrap.Extensions.StartupTasks;
    using Bootstrap.Unity;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Bootstrap.Bootstrapper.With.Unity().UsingAutoRegistration().And.StartupTasks().Start();
        }
    }
}
