﻿using System.Web;
using System.Web.Mvc;

namespace Iasset.TechnicalTest.WebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
