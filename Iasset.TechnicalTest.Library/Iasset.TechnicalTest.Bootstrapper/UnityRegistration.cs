﻿namespace Iasset.TechnicalTest.Bootstrapper
{
    using Bootstrap.Unity;

    using Iasset.TechnicalTest.Business;
    using Iasset.TechnicalTest.Business.Interfaces;

    using Microsoft.Practices.Unity;

    public class UnityRegistration : IUnityRegistration
    {
        public void Register(IUnityContainer container)
        {
            RegisterBusinessTypes(container);
        }

        private static void RegisterBusinessTypes(IUnityContainer container)
        {
            container.RegisterType<ICountryService, CountryService>();
            container.RegisterType<IWeatherService, WeatherService>();
        }
    }
}
