﻿namespace Iasset.TechnicalTest.Bootstrapper
{
    using System;
    using System.Web.Http;
    using Bootstrap.Extensions.StartupTasks;

    using Microsoft.Practices.Unity;

    public class CommonStartup : IStartupTask
    {
        public void Run()
        {
          var container = (IUnityContainer)Bootstrap.Bootstrapper.Container;

            // Web API resolver.
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
