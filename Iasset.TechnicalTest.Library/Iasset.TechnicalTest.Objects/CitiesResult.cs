﻿namespace Iasset.TechnicalTest.Objects
{
    using System.Runtime.Serialization;

    public class CitiesResult
    {
        [DataMember(Name = "Cities")]
        public City[] Cities { get; set; }
    }
}
