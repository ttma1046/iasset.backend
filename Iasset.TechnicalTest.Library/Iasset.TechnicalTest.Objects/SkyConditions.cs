﻿namespace Iasset.TechnicalTest.Objects
{
    using System.Runtime.Serialization;

    public class SkyConditions
    {
        [DataMember(Name = "Main")]
        public string Main { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }
    }
}
