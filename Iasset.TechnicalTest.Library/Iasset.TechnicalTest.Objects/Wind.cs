﻿namespace Iasset.TechnicalTest.Objects
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Wind : IMappable
    {
        [DataMember(Name = "Speed")]
        public double Speed { get; set; }

        [DataMember(Name = "Dep")]
        public double Dep { get; set; }
    }
}
