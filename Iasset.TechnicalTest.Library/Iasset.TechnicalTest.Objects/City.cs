﻿namespace Iasset.TechnicalTest.Objects
{
    using System.Runtime.Serialization;

    [DataContract]
    public class City : IMappable
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }
    }
}
