﻿namespace Iasset.TechnicalTest.Objects
{
    /// <summary>
    /// Interface to indicate the possibility to be mapped to another type.
    /// </summary>
    public interface IMappable
    {
    }
}
