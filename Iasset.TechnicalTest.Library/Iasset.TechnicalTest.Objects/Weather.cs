﻿namespace Iasset.TechnicalTest.Objects
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class Weather : IMappable
    {
        [DataMember(Name = "Location")]
        public string Location { get; set; }

        [DataMember(Name = "Time")]
        public DateTime Time { get; set; }

        [DataMember(Name = "Wind")]
        public Wind Wind { get; set; }

        [DataMember(Name = "Visibility")]
        public int Visibility { get; set; }

        [DataMember(Name = "SkyConditions")]
        public SkyConditions SkyConditions { get; set; }

        [DataMember(Name = "Temperature")]
        public double Temperature { get; set; }

        [DataMember(Name = "DewPoint")]
        public int DewPoint { get; set; }

        [DataMember(Name = "RelativeHumidity")]
        public int RelativeHumidity { get; set; }

        [DataMember(Name = "Pressure")]
        public int Pressure { get; set; }
    }
}
