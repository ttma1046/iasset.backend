﻿namespace Iasset.TechnicalTest.Business
{
    using System.IO;
    using System.Net;
    using System.Web.Script.Serialization;

    using Iasset.TechnicalTest.Objects;
    using Iasset.TechnicalTest.Business.Interfaces;

    public class CountryService: ICountryService
    {
        public CitiesResult GetCities(string countryName)
        {
            /*
             * The web services on webservicex.net does work.
             * so I create mock json file which will return mock data for names of the cities.
             */
            var request = (FileWebRequest)WebRequest.Create(System.Web.HttpContext.Current.Server.MapPath("~/Mock_Data/cities.json"));
            request.Method = "GET";
            request.ContentType = "application/json; charset=utf-8";

            using (var response = request.GetResponse())
            {
                using (var stream = new StreamReader(response.GetResponseStream()))
                {
                    var js = new JavaScriptSerializer();
                    var objText = stream.ReadToEnd();
                    var objResponse = (CitiesResult)js.Deserialize(objText, typeof(CitiesResult));

                    return objResponse;
                }
            }
        }
    }
}
