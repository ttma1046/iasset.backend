﻿namespace Iasset.TechnicalTest.Business.Interfaces
{
    using Iasset.TechnicalTest.Objects;

    public interface ICountryService
    {
        CitiesResult GetCities(string countryName);
    }
}
