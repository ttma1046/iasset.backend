﻿namespace Iasset.TechnicalTest.Business.Interfaces
{
    using Iasset.TechnicalTest.Objects;

    public interface IWeatherService
    {
        Weather GetWeatherByCity(string cityName);
    }
}
